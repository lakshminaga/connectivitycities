This is a spring boot web project which tells if two cities are connected

To build the project standard maven build is sufficient mvn clean install

To start/run the project spring boot maven plugin is included just run  spring-boot:run

It deployed as a Spring Boot APP and expose one endpoint: 

http://localhost:8081/Connected?origin=Trenton&destination= Albany
Result: Yes

http://localhost:8081/Connected?origin=Newark&destination= Boston
Result: Yes

http://localhost:8081/Connected?origin=Boston&destination= New York
Result: Yes

http://localhost:8081/Connected?origin=Phildelphia&destination= Newark
Result: Yes

http://localhost:8081/Connected?origin=Phildelphia&destination= Boston
Result: No
