package com.MasterCard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.springframework.stereotype.Component;

@Component
public class FileToMap {
 
private String filePath= "city.txt";
 private HashMap<String, String> filetomap = new HashMap<>();

 private void fileToMap() throws IOException {
	 
	String line;
	BufferedReader reader = new BufferedReader(new FileReader(filePath));
	while((line =reader.readLine())!= null) {
		String [] parts =line.split(",");
		if(parts.length >=2) {
			String name1 =parts[0];
			String name2=parts[1];
			filetomap.put(name1, name2);
		} else {
			System.out.println(line);
		}
	}
	
	for(String name1 :filetomap.keySet()) {
		System.out.println(name1+","+filetomap.get(name1));
	}
	reader.close();


}

	public String areCitiesConnected(String origin, String destination) throws IOException {
		
		fileToMap();
		
		if(filetomap.get(origin).equalsIgnoreCase(destination))
			return "Yes";
		return "No";
		


	}

	
	}
