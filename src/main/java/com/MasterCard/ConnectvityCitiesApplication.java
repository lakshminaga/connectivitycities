package com.MasterCard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConnectvityCitiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConnectvityCitiesApplication.class, args);
	}

}
